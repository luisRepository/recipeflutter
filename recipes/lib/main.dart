import 'package:flutter/material.dart';

import 'home.dart';

void main() {
  runApp(RecipeApp());
}

class RecipeApp extends StatelessWidget {
  final ThemeData theme = ThemeData();
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Recipe Calculator',
      theme: ThemeData(
        primarySwatch: Colors.grey,
        accentColor: Colors.black
      ),
      home: MyHomePage(),
    );
  }
}
